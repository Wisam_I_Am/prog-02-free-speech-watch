package com.fsm;

import android.database.Cursor;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import java.io.IOException;
import java.util.Random;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import android.content.Intent;

import com.gmail.yuyang226.flickrj.sample.android.FlickrHelper;
import com.gmail.yuyang226.flickrj.sample.android.FlickrjActivity;
import com.googlecode.flickrjandroid.Flickr;
import com.googlecode.flickrjandroid.FlickrException;
import com.googlecode.flickrjandroid.REST;
import com.googlecode.flickrjandroid.photos.Photo;
import com.googlecode.flickrjandroid.photos.PhotoList;
import com.googlecode.flickrjandroid.photos.PhotosInterface;
import com.googlecode.flickrjandroid.photos.SearchParameters;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.Constants;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.ResourceStoreException;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.ListCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.NotificationTextCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.SimpleTextCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.DeckOfCardsManager;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteDeckOfCards;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteDeckOfCardsException;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteResourceStore;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteToqNotification;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.resource.CardImage;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.resource.DeckOfCardsLauncherIcon;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.util.ParcelableUtil;

import org.json.JSONException;

import java.io.File;
import java.io.FileOutputStream;
import java.util.UUID;

import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;

import fsm.wisamreid.cs160.berkeley.edu.fsm.R;

public class LocationApp extends Activity {

    String[] activists = {"Art Goldberg", "Jack Weinberg", "Jackie Goldberg", "Joan Baez", "Mario Savio", "Michael Rossman"};
    String[] instructions = {"Draw \"Now\"", "Draw \"FSM\"", "Draw \"SLATE\"", "Draw a Megaphone", "Express your own view of Free Speech in a drawing", "Draw \"Free Speech\""};
    String[] cardIds = {"card.1", "card.2", "card.3", "card.4", "card.5", "card.6"};

    private final static String PREFS_FILE= "prefs_file";
    private final static String DECK_OF_CARDS_KEY= "deck_of_cards_key";
    private final static String DECK_OF_CARDS_VERSION_KEY= "deck_of_cards_version_key";

//    private View.OnClickListener mPickClickListener;
//    private View.OnClickListener mUploadClickListener;
//    private View.OnClickListener mRefreshClickListener;
    private View.OnClickListener mNoteClickListener;

    private DeckOfCardsManager mDeckOfCardsManager;
    private DeckOfCardsEventListener mDeckofCardEventListener;
    private RemoteDeckOfCards mRemoteDeckOfCards;
    private RemoteResourceStore mRemoteResourceStore;
    private CardImage[] mCardImages;
    private ToqBroadcastReceiver toqReceiver;
    private double final_distance = 0;
    private boolean sent = false;
    private File fileUri;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        mDeckOfCardsManager = DeckOfCardsManager.getInstance(getApplicationContext());
        toqReceiver = new ToqBroadcastReceiver();
        init();
        setupUI();

        mDeckofCardEventListener = new DeckOfCardsEventListener() {
            @Override
            public void onCardOpen(String s) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(LocationApp.this, MainActivity.class);
                        startActivity(intent);
                    }
                });
            }
            @Override
            public void onCardVisible(String s) {}

            @Override
            public void onCardInvisible(String s) {}

            @Override
            public void onCardClosed(String s) {}

            @Override
            public void onMenuOptionSelected(String s, String s2) {}

            @Override
            public void onMenuOptionSelected(String s, String s2, String s3) {}
        };

        final LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        // Define a listener that responds to location updates
        final LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();
                Location sproul = new Location("sproul");
                sproul.setLatitude(37.86965);
                sproul.setLongitude(-122.25914);
                double distance = location.distanceTo(sproul);

                if (distance <= 5000.0 && !sent) {
                    sendNotification();
                    sent = true;
                }
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {}

            public void onProviderEnabled(String provider) {}

            public void onProviderDisabled(String provider) {}
        };

        // Register the listener with the Location Manager to receive location updates
        if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            // Get update every 5 seconds
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 0, locationListener);
        }

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            // Get update every 5 seconds
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0, locationListener);
        }
    }

    /**
     * @see android.app.Activity#onStart()
     * This is called after onCreate(Bundle) or after onRestart() if the activity has been stopped
     */
    protected void onStart(){
        super.onStart();


        Log.d(Constants.TAG, "ToqApiDemo.onStart");
        // If not connected, try to connect
        if (!mDeckOfCardsManager.isConnected()){
            try{
                mDeckOfCardsManager.connect();
                mDeckOfCardsManager.addDeckOfCardsEventListener(mDeckofCardEventListener);
            }
            catch (RemoteDeckOfCardsException e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.submit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        // Handle item selection
        switch (item.getItemId()) {
            case R.id.refresh:
//                showImage();
                return true;

            case R.id.pick:
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.setFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                startActivityForResult(intent, 102);
                return true;

            case R.id.upload:

                if (fileUri == null) {
                    Toast.makeText(getApplicationContext(), "Please pick photo",
                            Toast.LENGTH_SHORT).show();

                    ImageView imview = (ImageView) findViewById(R.id.drawing);
                    Bitmap bitmap = ((BitmapDrawable)imview.getDrawable()).getBitmap();
                    Intent intent1 = new Intent(getApplicationContext(),
                            FlickrjActivity.class);
                    intent1.putExtra("flickImage", bitmap);
                    startActivity(intent1);
                } else {

                    Intent intent2 = new Intent(getApplicationContext(),
                            FlickrjActivity.class);
                    intent2.putExtra("flickImagePath", fileUri.getAbsolutePath());

                    startActivity(intent2);
                }
                return true;

            case R.id.notify:
                sendNotification();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupUI() {

        findViewById(R.id.install_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                install();
            }
        });

        findViewById(R.id.uninstall_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uninstall();
            }
        });
    }

    private void sendNotification() {
        Random random = new Random();
        String[] message = new String[1];
        message[0] = activists[random.nextInt(6)];

        // Create a NotificationTextCard
        NotificationTextCard notificationCard = new NotificationTextCard(System.currentTimeMillis(),
                "New Drawing Request!", message);

        // Draw divider between lines of text
        notificationCard.setShowDivider(true);
        // Vibrate to alert user when showing the notification
        notificationCard.setVibeAlert(true);
        // Create a notification with the NotificationTextCard we made
        RemoteToqNotification notification = new RemoteToqNotification(this, notificationCard);

        try {
            // Send the notification
            mDeckOfCardsManager.sendNotification(notification);
            Toast.makeText(this, "Sent Notification", Toast.LENGTH_SHORT).show();
        } catch (RemoteDeckOfCardsException e) {
            e.printStackTrace();
//            Toast.makeText(this, "Failed to send Notification", Toast.LENGTH_SHORT).show();
        }
    }
    /**
     * Installs applet to Toq watch if app is not yet installed
     */
    private void install() {
        boolean isInstalled = true;

        try {
            isInstalled = mDeckOfCardsManager.isInstalled();
        }
        catch (RemoteDeckOfCardsException e) {
            e.printStackTrace();
            Toast.makeText(this, "Error: Can't determine if app is installed", Toast.LENGTH_SHORT).show();
        }

        if (!isInstalled) {
            try {
                mDeckOfCardsManager.installDeckOfCards(mRemoteDeckOfCards, mRemoteResourceStore);
            } catch (RemoteDeckOfCardsException e) {
                e.printStackTrace();
                Toast.makeText(this, "Error: Cannot install application", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "App is already installed!", Toast.LENGTH_SHORT).show();
        }

        try{
            storeDeckOfCards();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private void uninstall() {
        boolean isInstalled = true;

        try {
            isInstalled = mDeckOfCardsManager.isInstalled();
        }
        catch (RemoteDeckOfCardsException e) {
            e.printStackTrace();
            Toast.makeText(this, "Error: Can't determine if app is installed", Toast.LENGTH_SHORT).show();
        }

        if (isInstalled) {
            try{
                mDeckOfCardsManager.uninstallDeckOfCards();
            }
            catch (RemoteDeckOfCardsException e){
                Toast.makeText(this, getString(R.string.error_uninstalling_deck_of_cards), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, getString(R.string.already_uninstalled), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Adds a deck of cards to the applet
     */
    private void addSimpleTextCard(String name, String msg, String id) {
        ListCard listCard = mRemoteDeckOfCards.getListCard();
        int currSize = listCard.size();

        // Create a SimpleTextCard with 1 + the current number of SimpleTextCards
        SimpleTextCard simpleTextCard = new SimpleTextCard(Integer.toString(currSize+1));
        String header = "FSM";
        simpleTextCard.setHeaderText(header);
        simpleTextCard.setTitleText(name);
        String[] messages = {msg};
        simpleTextCard.setMessageText(messages);
        simpleTextCard.setReceivingEvents(true);
        simpleTextCard.setShowDivider(true);
        try {
            simpleTextCard.setCardImage(mRemoteResourceStore, (CardImage) mRemoteResourceStore.getResource(id));
        }
        catch (ResourceStoreException e) {
            e.printStackTrace();
        }

        listCard.add(simpleTextCard);

        try {
            mDeckOfCardsManager.updateDeckOfCards(mRemoteDeckOfCards, mRemoteResourceStore);
        } catch (RemoteDeckOfCardsException e) {
            e.printStackTrace();
//            Toast.makeText(this, "Failed to Create SimpleTextCard", Toast.LENGTH_SHORT).show();
        }
    }


    private void removeDeckOfCards() {
        ListCard listCard = mRemoteDeckOfCards.getListCard();
        if (listCard.size() == 0) {
            return;
        }

        listCard.remove(0);

        try {
            if (listCard.size() != 0) {
                mDeckOfCardsManager.updateDeckOfCards(mRemoteDeckOfCards, mRemoteResourceStore);
            }
        } catch (RemoteDeckOfCardsException e) {
            e.printStackTrace();
//            Toast.makeText(this, "Failed to delete Card from ListCard", Toast.LENGTH_SHORT).show();
        }

    }

    // Initialise
    private void init(){

        // Create the resource store for icons and images
        mRemoteResourceStore= new RemoteResourceStore();

        DeckOfCardsLauncherIcon whiteIcon = null;
        DeckOfCardsLauncherIcon colorIcon = null;

        // Get the launcher icons
        try{
            whiteIcon= new DeckOfCardsLauncherIcon("white.launcher.icon", getBitmap("bw.png"), DeckOfCardsLauncherIcon.WHITE);
            colorIcon= new DeckOfCardsLauncherIcon("color.launcher.icon", getBitmap("color.png"), DeckOfCardsLauncherIcon.COLOR);
        }
        catch (Exception e){
            e.printStackTrace();
            System.out.println("Can't get launcher icon");
            return;
        }

        mCardImages = new CardImage[6];
        try{
            mCardImages[0]= new CardImage("card.image.1", getBitmap("art_goldberg_toq.png"));
            mCardImages[1]= new CardImage("card.image.2", getBitmap("jack_weinberg_toq.png"));
            mCardImages[2]= new CardImage("card.image.3", getBitmap("jackie_goldberg_toq.png"));
            mCardImages[3]= new CardImage("card.image.4", getBitmap("joan_baez_toq.png"));
            mCardImages[4]= new CardImage("card.image.5", getBitmap("mario_savio_toq.png"));
            mCardImages[5]= new CardImage("card.image.6", getBitmap("michael_rossman_toq.png"));


        }
        catch (Exception e){
            e.printStackTrace();
            System.out.println("Can't get picture icon");
            return;
        }

        // Try to retrieve a stored deck of cards
        try {
            // If there is no stored deck of cards or it is unusable, then create new and store
            if ((mRemoteDeckOfCards = getStoredDeckOfCards()) == null){
                mRemoteDeckOfCards = createDeckOfCards();
                storeDeckOfCards();
            }
        }
        catch (Throwable th){
            th.printStackTrace();
            mRemoteDeckOfCards = null; // Reset to force recreate
        }

        // Make sure in usable state
        if (mRemoteDeckOfCards == null){
            mRemoteDeckOfCards = createDeckOfCards();
        }

        // Set the custom launcher icons, adding them to the resource store
        mRemoteDeckOfCards.setLauncherIcons(mRemoteResourceStore, new DeckOfCardsLauncherIcon[]{whiteIcon, colorIcon});


        mRemoteResourceStore.addResource(mCardImages[0]);
        mRemoteResourceStore.addResource(mCardImages[1]);
        mRemoteResourceStore.addResource(mCardImages[2]);
        mRemoteResourceStore.addResource(mCardImages[3]);
        mRemoteResourceStore.addResource(mCardImages[4]);
        mRemoteResourceStore.addResource(mCardImages[5]);

        int card_size = mRemoteDeckOfCards.getListCard().size();
        for (int i = 0; i< card_size; i++) {
            removeDeckOfCards();
        }

        for (int my_int = 0; my_int< 6; my_int++) {
            addSimpleTextCard(activists[my_int], instructions[my_int], cardIds[my_int]);
        }

    }

    // Read an image from assets and return as a bitmap
    private Bitmap getBitmap(String fileName) throws Exception{

        try{
            InputStream is= getAssets().open(fileName);
            return BitmapFactory.decodeStream(is);
        }
        catch (Exception e){
            throw new Exception("An error occurred getting the bitmap: " + fileName, e);
        }
    }

    private RemoteDeckOfCards getStoredDeckOfCards() throws Exception{

        if (!isValidDeckOfCards()){
            Log.w(Constants.TAG, "Stored deck of cards not valid for this version of the demo, recreating...");
            return null;
        }

        SharedPreferences prefs= getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE);
        String deckOfCardsStr= prefs.getString(DECK_OF_CARDS_KEY, null);

        if (deckOfCardsStr == null){
            return null;
        }
        else{
            return ParcelableUtil.unmarshall(deckOfCardsStr, RemoteDeckOfCards.CREATOR);
        }

    }

    /**
     * Uses SharedPreferences to store the deck of cards
     * This is mainly used to
     */
    private void storeDeckOfCards() throws Exception{
        // Retrieve and hold the contents of PREFS_FILE, or create one when you retrieve an editor (SharedPreferences.edit())
        SharedPreferences prefs = getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE);
        // Create new editor with preferences above
        SharedPreferences.Editor editor = prefs.edit();
        // Store an encoded string of the deck of cards with key DECK_OF_CARDS_KEY
        editor.putString(DECK_OF_CARDS_KEY, ParcelableUtil.marshall(mRemoteDeckOfCards));
        // Store the version code with key DECK_OF_CARDS_VERSION_KEY
        editor.putInt(DECK_OF_CARDS_VERSION_KEY, Constants.VERSION_CODE);
        // Commit these changes
        editor.commit();
    }

    // Check if the stored deck of cards is valid for this version of the demo
    private boolean isValidDeckOfCards(){

        SharedPreferences prefs= getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE);
        // Return 0 if DECK_OF_CARDS_VERSION_KEY isn't found
        int deckOfCardsVersion= prefs.getInt(DECK_OF_CARDS_VERSION_KEY, 0);

        return deckOfCardsVersion >= Constants.VERSION_CODE;
    }

    // Create some cards with example content
    private RemoteDeckOfCards createDeckOfCards(){

        ListCard listCard= new ListCard();
        //System.out.println("the size is " + Integer.toString(listCard.size()));

        SimpleTextCard simpleTextCard= new SimpleTextCard("card0");
        listCard.add(simpleTextCard);
        SimpleTextCard simpleTextCard1= new SimpleTextCard("card1");
        listCard.add(simpleTextCard1);


        return new RemoteDeckOfCards(this, listCard);
    }


//    // FlickrTest code from Section
//    private void showImage() {
//        Thread thread = new Thread() {
//            @Override
//            public void run() {
//                try {
//                    String svr="www.flickr.com";
//
//                    REST rest=new REST();
//                    rest.setHost(svr);
//
//                    //initialize Flickr object with key and rest
//                    Flickr flickr=new Flickr(FlickrHelper.API_KEY,rest);
//
//                    //initialize SearchParameter object, this object stores the search keyword
//                    SearchParameters searchParams=new SearchParameters();
//                    searchParams.setSort(SearchParameters.INTERESTINGNESS_DESC);
//
//                    //Create tag keyword array
//                    String[] tags=new String[]{"cs160_fall2014"};
//                    searchParams.setTags(tags);
//
//                    //Initialize PhotosInterface object
//                    PhotosInterface photosInterface=flickr.getPhotosInterface();
//                    //Execute search with entered tags
//                    PhotoList photoList=photosInterface.search(searchParams,20,1);
//
//                    //get search result and fetch the photo object and get small square imag's url
//                    if(photoList!=null){
//                        //Get search result and check the size of photo result
//                        Random random = new Random();
//                        int seed = random.nextInt(photoList.size());
//                        //get photo object
//                        Photo photo=(Photo)photoList.get(seed);
//
//                        //Get small square url photo
//                        InputStream is = photo.getMediumAsStream();
//                        final Bitmap bm = BitmapFactory.decodeStream(is);
//
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                ImageView imageView = (ImageView) findViewById(R.id.drawing);
//                                imageView.setImageBitmap(bm);
//                            }
//                        });
//                    }
//                } catch (ParserConfigurationException e) {
//                    e.printStackTrace();
//                } catch (FlickrException e) {
//                    e.printStackTrace();
//                } catch (IOException e ) {
//                    e.printStackTrace();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        };
//
//        thread.start();
//    }

    public void onResume() {
        super.onResume();
//        showImage();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 102) {

            if (resultCode == Activity.RESULT_OK) {
                Uri tmp_fileUri = data.getData();

                ((ImageView) findViewById(R.id.drawing))
                        .setImageURI(tmp_fileUri);

                String selectedImagePath = getPath(tmp_fileUri);
                fileUri = new File(selectedImagePath);
            }

        }
    };

    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        @SuppressWarnings("deprecation")
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

}
